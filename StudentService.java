package com.example.springbootsecurity.services;

import com.example.springbootsecurity.models.City;
import com.example.springbootsecurity.models.Student;
import com.example.springbootsecurity.models.Teacher;

import java.util.List;
import java.util.Optional;

/**
 * Service to handle student related operations
 *
 * @author Vinod John
 */

public interface StudentService {

    /**
     * To find all students
     *
     * @return List of Student
     */
    List<Student> findAllStudents();

    /**
     * To find all students by city
     *
     * @param city City
     * @return List of Student
     */
    List<Student> findAllStudentsByCity(City city);

    /**
     * To create a new student
     *
     * @param student Student
     */
    void createStudent(Student student);

    /**
     * To find student by Id
     *
     * @param id Id of the student
     * @return Optional of Student
     */

    Optional<Student> findStudentById(Long id);

    /**
     * To update a student
     *
     * @param student Student
     */
    void updateStudent(Student student);

    /**
     * To delete a student by Id
     *
     * @param id Id of the student
     */
    void deleteStudentById(Long id);

    /**
     * To restore a student by Id
     *
     * @param id Id of the student
     */
    void restoreStudentById(Long id);

}
