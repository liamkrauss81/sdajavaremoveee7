package com.example.springbootsecurity.models;

import com.example.springbootsecurity.models.City;
import com.example.springbootsecurity.models.School;
import com.example.springbootsecurity.models.Speciliazation;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * Student model
 *
 * @author Liam Krauss
 */

@Data
@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private String id;
    private String firstname;
    private String lastname;
    private String description;

    @OneToOne(cascade = CascadeType.MERGE)
    private City city;

    @Enumerated(EnumType.STRING)
    private School school;

    @Enumerated(EnumType.STRING)
    private Speciliazation speciliazation;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate joinDate;

    private boolean isActive;

    {
    }
}
