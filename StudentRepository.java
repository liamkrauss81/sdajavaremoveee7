package com.example.springbootsecurity.respositories;


import com.example.springbootsecurity.models.City;
import com.example.springbootsecurity.models.Student;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository to handle DB operations for Student
 *
 * @author Vinod John
 */
@Repository

public interface StudentRepository extends JpaRepository<Student, Long> {
   List<Student> findAllByCity(City city);
}
